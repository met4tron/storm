import React from "react";
import logo from "./logo.svg";
import "./App.scss";
import ListUsers from "./views/ListUsers";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";

function App() {
  return (
    <div className="App">
      <Navbar />
      <ListUsers />
      <Footer />
    </div>
  );
}

export default App;
