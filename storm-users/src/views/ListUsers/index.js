import React from "react";
import Container from "../../components/Container";
import Table from "../../components/Table";

const ListUsers = () => {
  return (
    <Container>
      <Table />
    </Container>
  );
};

export default ListUsers;
