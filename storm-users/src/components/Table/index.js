import React, { useState, useEffect } from "react";
import { format } from "date-fns";
import ReactLoading from "react-loading";
import Table from "react-bulma-components/lib/components/table";
import Skeleton from "react-loading-skeleton";

import Button from "react-bulma-components/lib/components/button";

const TableComponent = () => {
  const [allUsers, setAllUsers] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const getUsers = async () => {
      try {
        const response = await fetch(
          "https://5e57da2a4c695f00143301a5.mockapi.io/users"
        );
        const responseJson = await response.json();

        setAllUsers(responseJson);
        setLoading(false);
      } catch (error) {
        console.log(error);
      }
    };

    getUsers();
  }, []);

  const formatDate = dt => format(new Date(dt), "dd/MM/yyyy");

  const renderPlaceholder = () => {
    const tempArray = Array(10).fill(0);

    return tempArray.map(x => {
      console.log("Render");
      return (
        <tr>
          <th className="has-text-centered">
            <Skeleton />
          </th>
          <td>
            <Skeleton />
          </td>
          <td>
            <Skeleton />
          </td>
          <td>
            <Skeleton />
          </td>
          <td>
            <Skeleton />
          </td>
          <td>
            <Skeleton />
          </td>
          <td>
            <Skeleton />
          </td>
          <td>
            <Skeleton />
          </td>
        </tr>
      );
    });
  };

  const renderRow = users => {
    return users.map(user => {
      return (
        <tr key={user.name}>
          <th className="has-text-centered">
            <label className="checkbox ">
              <input type="checkbox" />
            </label>
          </th>
          <td>{user.name}</td>
          <td>{user.email}</td>
          <td>{formatDate(user.createdAt)}</td>
          <td>{formatDate(user.createdAt)}</td>
          <td>{user.rules}</td>
          <td>
            {user.status ? (
              <p className="has-text-success has-text-weight-bold">Active</p>
            ) : (
              <p className="has-text-danger has-text-weight-bold">Inativo</p>
            )}
          </td>
          <td>
            <Button color="primary has-text-weight-bold">Ações</Button>
          </td>
        </tr>
      );
    });
  };

  return (
    <Table>
      <thead>
        <tr>
          <th>Selecione</th>
          <th>Usuário</th>
          <th>Email</th>
          <th>Data de Inclusão</th>
          <th>Data de Alteração</th>
          <th>Regras</th>
          <th>Status</th>
          <th>Ações</th>
        </tr>
      </thead>

      <tbody>
        {allUsers.length ? renderRow(allUsers) : renderPlaceholder()}
      </tbody>
    </Table>
  );
};

export default TableComponent;
