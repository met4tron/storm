import React from "react";
import { getSeconds, getMinutes, getHours, format } from "date-fns";
import { ptBR } from "date-fns/locale";
import Footer from "react-bulma-components/lib/components/footer";

const FooterComponent = () => {
  const [realtime, setRealtime] = React.useState("");

  React.useEffect(() => {
    const interval = setInterval(() => {
      let seconds = getSeconds(new Date());
      let minutes = getMinutes(new Date());
      let hours = getHours(new Date());

      if (seconds < 10) {
        seconds = `0${seconds}`;
      }

      if (minutes < 10) {
        minutes = `0${minutes}`;
      }

      if (hours < 10) {
        hours = `0${hours}`;
      }

      setRealtime(`${hours}:${hours}:${seconds}`);
    }, 1000);

    return () => clearInterval();
  }, []);

  return (
    <Footer>
      <div className="available">NO AR</div>
      <div>Encontro - 10:00</div>
      <div>Última Atualização em 10:28</div>
      <div>
        {format(new Date(), "iiii, dd 'de' MMMM 'de' yyyy", {
          locale: ptBR
        })}
      </div>
      <div className="countdown">\\ {realtime}</div>
    </Footer>
  );
};
export default FooterComponent;
