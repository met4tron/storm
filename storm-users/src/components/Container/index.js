import React from "react";
import Container from "react-bulma-components/lib/components/container";
import Section from "react-bulma-components/lib/components/section";

const ContainerComponent = ({ children }) => {
  return (
    <Section>
      <Container fluid>{children}</Container>
    </Section>
  );
};

export default ContainerComponent;
