import React from "react";
import Navbar from "react-bulma-components/lib/components/navbar";
import Button from "react-bulma-components/lib/components/button";
import { FontAwesomeIcon as Icon } from "@fortawesome/react-fontawesome";
import {
  faCog,
  faSignOutAlt,
  faHome,
  faSlidersH,
  faUser
} from "@fortawesome/free-solid-svg-icons";

const NavbarComponent = () => {
  return (
    <Navbar fixed="top" isSpaced>
      <Navbar.Brand>
        <Navbar.Item renderAs="a" href="#">
          TV Globo
        </Navbar.Item>
        <Navbar.Burger />
      </Navbar.Brand>
      <Navbar.Container position="end">
        <Navbar.Item href="#">
          <Button>
            <Icon icon={faSlidersH} />
          </Button>
        </Navbar.Item>
        <Navbar.Item href="#">
          <Button color="primary">
            <Icon icon={faUser} className="icon-navbar-button" />
            Adicionar Usuário
          </Button>
        </Navbar.Item>
        <Navbar.Item href="#">
          <Icon icon={faHome} />
        </Navbar.Item>
        <Navbar.Item href="#">
          <Icon icon={faCog} />
        </Navbar.Item>
        <Navbar.Item href="#">
          <Icon icon={faSignOutAlt} />
        </Navbar.Item>
      </Navbar.Container>
    </Navbar>
  );
};

export default NavbarComponent;
