const openBrackets = ["(", "{", "["];
const closeBrackets = [")", "}", "]"];

const reduceChars = arr => {
  return !arr.reduce((previous, now) => {
    if (openBrackets.includes(now)) {
      return (previous = previous + 1);
    }

    if (closeBrackets.includes(now)) {
      return (previous = previous - 1);
    }

    return previous;
  }, 0);
};

function checkBalance(str) {
  const withoutSpaces = str.split("");

  if (withoutSpaces.length) {
    console.log(
      `Os characteres ${str} estão balanceados? ${reduceChars(withoutSpaces)}`
    );
  } else {
    console.log("Não Balanceado");
  }
}

checkBalance("[()]{}{[()()]()}");
checkBalance("[()]{}{[()()]()}");
checkBalance("[{({[()]})}]");
checkBalance("[()]{}{[()(]()}");
checkBalance("[()]{}{[()(]()}");
checkBalance("[()]{}{[]()()]()}");
checkBalance("[()]{}{[()(]()}");
checkBalance("[({}())]");
checkBalance("[()]{}{[(()][(}{][()]})])(]()}");
